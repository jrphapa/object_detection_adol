
# coding: utf-8

# # Object Detection Demo
# Welcome to the object detection inference walkthrough!  This notebook will walk you step by step through the process of using a pre-trained model to detect objects in an image. Make sure to follow the [installation instructions](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md) before you start.

# # Imports

# In[1]:


import numpy as np
import os
import six.moves.urllib as urllib
import sys
#import thread
import tarfile
import tensorflow as tf
import zipfile
from datetime import datetime
import sqlite3
from multiprocessing import Process
from multiprocessing import Lock
from functools import partial
from subprocess import call

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import cv2

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops

if tf.__version__ < '1.4.0':
  raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')


# ## Env setup

# In[ ]:


# This is needed to display the images.
#get_ipython().magic(u'matplotlib inline')


# ## Object detection imports
# Here are the imports from the object detection module.

# In[ ]:


from utils import label_map_util

from utils import visualization_utils as vis_util


# # Model preparation 

# ## Variables
# 
# Any model exported using the `export_inference_graph.py` tool can be loaded here simply by changing `PATH_TO_CKPT` to point to a new .pb file.  
# 
# By default we use an "SSD with Mobilenet" model here. See the [detection model zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md) for a list of other models that can be run out-of-the-box with varying speeds and accuracies.

# In[ ]:

#CWD_PATH = os.getcwd()
# What model to download.
#MODEL_NAME = 'ssd_mobilenet_v1_coco_2017_11_17'
MODEL_NAME = 'faster_rcnn_resnet101_coco_2018_01_28'
#MODEL_NAME = 'faster_rcnn_inception_v2_coco_2018_01_28'
#MODEL_NAME = 'inception-2015-12-05'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

# ## Download Model

# In[ ]:


#opener = urllib.request.URLopener()
#opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
tar_file = tarfile.open(MODEL_FILE)
for file in tar_file.getmembers():
  file_name = os.path.basename(file.name)
  if 'frozen_inference_graph.pb' in file_name:
    tar_file.extract(file, os.getcwd())


# ## Load a (frozen) Tensorflow model into memory.

# In[ ]:


detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


# ## Loading label map
# Label maps map indices to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine

# In[ ]:


label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# ## Helper code

# In[ ]:


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


# # Detection

# In[ ]:


# For the sake of simplicity we will use only 2 images:
# image1.jpg
# image2.jpg
# If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
#PATH_TO_TEST_IMAGES_DIR = 'test_images'
PATH_TO_TEST_IMAGES_DIR = 'input_images'
#TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.jpg'.format(i)) for i in range(1, #3) ]


# Size, in inches, of the output images.
IMAGE_SIZE = (12, 8)


# In[ ]:


def run_inference_for_single_image(image, graph):
  with graph.as_default():
    with tf.Session() as sess:
      # Get handles to input and output tensors
      ops = tf.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
      ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
              tensor_name)
      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: np.expand_dims(image, 0)})

      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict[
          'detection_classes'][0].astype(np.uint8)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
  return output_dict














###################################   ADOL IMPLEMENTATION - OBJ DETECT ###################################
# don't forget to source ~/tensorflow/bin/activate before running

# 1. save image to file and 2. save image information to database 

# TODO: change these to send to firebase and notify !!!
# 1.
def save_image_to_file(image_index):
  #save image to output file by copying from input directory
  target_filename = str(datetime.now().time())+".png"
  target_path = "output_images/"+target_filename
  imToSave = "input_images/im"+image_index+".png"
  call(["cp", imToSave, target_path])
  return target_path


# TODO: change these to send to firebase and notify !!!
# 2.
def save_recognized_data(target_path, lock):
  lock.acquire()
  now = "'"+str(datetime.now().time())+"'"   #convert into TEXT format in database
  destination = os.getcwd()
  filename = "'"+destination+"/"+target_path+"'"
  lock.release()
  # TODO: change position to current pos !!!
  position = "'here'"
  conn = sqlite3.connect('/home/adol/adol.db')
  cur = conn.cursor()
  # TABLE im_output (Id INTEGER PRIMARY KEY,Path TEXT,Position TEXT, Time TEXT);
  statement = "INSERT INTO im_output(Path,Position,Time) VALUES(" + filename + "," + position + "," + now + ")"
  cur.execute(statement)
  conn.commit()
  conn.close() 

# In[ ]:





#image index start at 20
i=20
# TODO: get target object name from ios app 
target_obj = "bottle"
# threading variable
lock = Lock()

while True:

  ###################### get image from input directory ################################!!!!!

  print("i ",i)
  image = Image.open(os.path.join(PATH_TO_TEST_IMAGES_DIR, 'im{}.png'.format(i)))


  ##################### tensorflow stuffs ##################

  # the array based representation of the image will be used later in order to prepare the
  # result image with boxes and labels on it.

  image_np = load_image_into_numpy_array(image)

  # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
  image_np_expanded = np.expand_dims(image_np, axis=0)
  # Actual detection.
  output_dict = run_inference_for_single_image(image_np, detection_graph)
  # Visualization of the results of a detection.
  vis_util.visualize_boxes_and_labels_on_image_array(
      image_np,
      output_dict['detection_boxes'],
      output_dict['detection_classes'],
      output_dict['detection_scores'],
      category_index,
      instance_masks=output_dict.get('detection_masks'),
      use_normalized_coordinates=True,
      line_thickness=8)\



################## create object list and check if recognized target object ##################
# format: [{'prob': 0.9362355, 'name': u'sheep'}, {'prob': 0.35480633, 'name': u'cake'}]

  n = output_dict['num_detections']
  #obj_list = list()
  for j in range(n):
        index = output_dict['detection_classes'][j]
        label_set = category_index.get(index)
        label = label_set.get('name')
	print("label: ",label)
        probability = output_dict['detection_scores'][j]
	# keep list of objects found 
        #obj_dict = dict(name=label,prob=probability)
        #obj_list.append(obj_dict)
        #print(obj_list)

	# notify whenever obj found and save to DB
        # TODO: call function to save to firebase, notify
	if(label.find(target_obj)!=-1):
	   print("found")  
           target_path = save_image_to_file(str(i))
	   Process(target=save_recognized_data, args=(target_path, lock)).start()
  i+=20
  if(i>5000):
     i=0
  
  cv2.imshow('result',image_np)










##################################################
  #if(cv2.waitKey(25) & 0xFF == ord('q')):
	#cv2.destroyAllWindows()
	#break
  #plt.figure(figsize=IMAGE_SIZE)
  #plt.imshow(image_np)

##############################
  #ret,image_np = cap.read()
  #image_np = cap
  #for image_path in TEST_IMAGE_PATHS:
  #image = Image.open(image_path)
  #TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.jpg'.format(i)) for i in range(1, #3) ]
#cv2.imshow('result',cv2.resize(image_np,(800,600)))


