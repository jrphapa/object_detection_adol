import cv2
import urllib
import numpy as np
import os
from time import sleep
from Queue import Queue
from threading import Thread

#stream = urllib.urlopen('http://88.53.197.250/axis-cgi/mjpg/video.cgi?resolution=320x240')
#cap = cv2.VideoCapture('http://82.89.169.171/axis-cgi/mjpg/video.cgi?resolution=320x240')

# at ic
cap = cv2.VideoCapture('rtsp://192.168.95.204:8554/unicast')
# at iphone
# cap = cv2.VideoCapture('rtsp://172.20.10.13:8554/unicast')
#bytes=''
#while True:
#	bytes+=stream.read(1024)
#	a = bytes.find('\xff\xd8')
#	b = bytes.find('\xff\xd9')
#	if a!=-1 and b!=-1:
#		jpg = bytes[a:b+2]
#		bytes =  bytes[b+2:]
#		i =  cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_COLOR)
#		cv2.imshow('i',i)
#		if(cv2.waitKey(1) == 27):
#			exit(0)
i=1
#dirname = './tensorflow/models/research/object_detection/input_images/'
dirname = '/home/adol/tensorflow/models/research/object_detection/input_images/'


def ImSave(q):
   while True:
     if not q.empty():
     	item = q.get()
     	file_name = item[0]
     	image = item[1]
     	cv2.imwrite(os.path.join(dirname,file_name),image)
     	q.task_done()

q = Queue(maxsize=0)
num_threads = 10

for i in range(num_threads):
    worker = Thread(target=ImSave,args=(q,))
    worker.setDaemon(True)
    worker.start()


while True:
	ret,image = cap.read()
	file_name = 'im'+str(i)+'.png'
	#cv2.imwrite(os.path.join(dirname,file_name),image)
	#sleep(0.1)
	#cv2.imshow('output',image)
        data = [file_name,image]
        if(i % 20 == 0):
           q.put(data)
        i+=1
        print("i:",i)
	if(i>5000):
          i=0
	#if(cv2.waitKey(1)>=0 ):
		#break

q.join()
